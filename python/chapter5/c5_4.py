"""
Approach 1 (Brute force): Add/Subtract by 1, check if the number of 1s in the
binary representation are the same.

Approach 2: Starting with the next larger number. We'll need to flip some 0s
to 1s and 1s to 0s but we need to make sure the 0s are at a higher significant
figure to ensure the number is larger. We also want to make sure this greater
number is still as small as possible so that it''s the next possible number.

For this reason the 0 that we turn to a 1 needs to be at a position such that
there are 1s below it that we can flip to 0s. But only flipping those 1s to 0s
may not be enough as the next value might still have a 1 in positions that were
1s.

So far we know that we want to select the first 0 reading from right,
at least one 1 after it.
After selecting that 0,  we want to remove one of the 1s to the right of it and
at the same time ensure the remaining 1s are at low of a position as possible.

Due to the characteristics of the chosen 0, there's at most one contiguous
sequence of 0s after it.
some examples where the  the chosen 0 is marked as x:
* 10100: 1x100
* 101010: 101x10
* 1010011: 1010x11
* 10100100: 1010x100
To check these, look at the increasing series starting from the first element
and check that in the answer the 0s marked with x will be changed to 1

So to ensure the 1s after the selected 0 are at lowest position and one of them
is removed we can simply shift them down until one 1 is removed. (Again due to
properties of the selected 0 we know that there is a restriction on the
sequences of 0s and 1s, namely only 1 possible sequence of 0s will exist, hence
the shifting will always work) While shifting down, the empty spots must be
filled with 0


For getting the next smaller number, we need to flip a 1 to zero such that it
has 0s below it (as in 0s to the right of it, lower significant positions) that
we can flip to 1s. We need to flip only one 0 to 1 and the resulting value
needs to remain the next smaller, so flip the 0 in the most significant
position after the chosen 1.


^ A few incorrect steps (due to time limitation will fix later :( )
E.g. shifting is not the correct, for get_next count the 1s after the marked
zero, let it be c1, and add c1-1 1s at the end
For get_prev count the 1s as well, c1, but add c1+1 1s right after (to the
right) of the marked one. [The marked zero/one is the one that got chosen and
flipped initially]
"""