"""
I assume I need to return something like 0.10101
We have only 30 characters to represent the number (the first two are used for
`0.`)

A number such as 0.101101 represents 1*2^-1 + 0*2^-2 + 1*2^-3 + 1*2^-4 + ...

We can repeatedly multiply by 2, in the ith iteration if the number is greater
than 1, the coefficient of that term, 2^-i, is 1, and we subtract 1 and
continue until the number is 0. If we the number is less than 1, the
coefficient of that term is 0; we continue the process on multiplying by 2
until the number is 0.

If while we are multiplying by 2, the number of terms exceeds 30 we terminate
and print "ERROR"


"""
def binary_to_string(n):
    if n >= 1 or n <= 0:
        print("Invalid input")
        return 0
    d = []
    for i in range(0,30):
        n = n * 2
        if n >= 1:
            d.append("1")
            n -= 1
        else:
            d.append("0")

        if n == 0:
            break
    if n != 0:
        print("ERROR")
        return 0
    return "0."+''.join(d)

print(binary_to_string(0.5))
print(binary_to_string(0.625))
print(binary_to_string(2**-12))
print(binary_to_string(2**-30))
print(binary_to_string(2**-31)) # ERROR
print(binary_to_string(1/3)) # ERROR
