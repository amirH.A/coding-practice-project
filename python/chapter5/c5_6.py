"""
Approach 1: Convert both numbers to binary, start from the right and move left
on both simultaneously and compare the values at the corresponding positions,
if not the same, we need to flip one, so add 1 to a counter. Do this until the
longer value has no more bits (we may need to flip leading 0s in the smaller
number)

Approach 2: A simple improvement on above:
1. Mask both numbers with 1 and comapre, if not the same add 1 to the counter
2. Shift both numbers down by 1
3. Go to 1 until both numbers are 0

Book solution: XOR the numbers and count the 1s. First approach which shifting
but an improved version which clears the least significant bit is also shown,
where a binary c is set to c & (c-1) [c = c & (c - 1)] until c is 0, this will
iterate only as many times as there are 1s in c, which could be faster but not
asymptotically, though a very nice trick.
"""

def conversion(a, b):
    conversions = 0
    while not (a == 0 and b == 0):
        conversions += ((a & 1) != (b & 1))
        a >>= 1
        b >>= 1

    return conversions

print(conversion(2**5, 0)) # 1 (Notice that this behaviour can be changed by
# terminating when one of the values is 0 and then adding however many bits
# there are left in the other value. But I think this is also desirable as 0
# is virtually represented as 00...0 by this algorithm)
print(conversion(2**5, 2**5 - 1)) # 6
print(conversion(0b111000, 0b10110)) # 4
print(conversion(29, 15)) # 2