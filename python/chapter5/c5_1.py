"""
The questions seems to have requested a side effect as well other than the
insertion. We start at bit j and end at bit i, and are told there's at least
enough space for M. So if the space between j and i is greater than length of
M should it be filled with 0s/1s or left untouched?
Relevant part of the question: "Write a method to insert M into N such that M
starts at bit j and ends at bit i"

I think the desired effect in case that there's more space than length of M, is
to 0 pad M and make sure the last bit of M is at i. M is given as a 32-bit
integer so it will already be 0-padded (This also solves the ambiguity of where
the actual value of M begins and ends, its end is the same as end of the 32 bit
number, its start is dictated by i and j)

Clear the bits from j to i and then OR them with relevant part of M
"""

def insertInTo(N, M, i, j):
    # We are not given the 'actual' length of M so we have to 0-pad
    # but be careful not to overwrite

    # We want a mask of form 111100001111 where 0s start and end according to i
    # and j.
    left = -1 << (j+1)   # 11110000
    right = (1 << i) - 1 # 00001111
    mask = left | right
    N = N & mask
    return N | (M << i)

# insertInTo(1,1,2,6)
# print("{0:32b}".format(mask))