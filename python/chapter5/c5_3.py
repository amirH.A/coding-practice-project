"""
Approach 1: Convert to binary string. Start a counter of number of ones seen so
far. When we come across the first 0, record its index Assume this 0 has been turned to a 1 and keep counting the 1s until
reaching the next 0.
Record the position of this 0 and hold the position of
the previous 0 as well.
Record the count of 1s seen so far as max_count.

Assume the last 0 was not turned to a 1 and this one is now turned to a 1 and
keep recording the number of 1s seen. The counter will start at the number of
1s between the previous 0 and this one which will be (index of this 0 - index
of prev 0 -1). Then we add one due to the current 0 being turned to 1, so
current_count will be modified to index of this 0 - prev 0's index

Continue the count until the next 0, update max_count only if the current count
is higher. Repeat until the end of string is reached

Space: O(1) Time: O(n) where n is the number of chars in the string i.e. log N
where N is the number

After reading the book solution: My solution seems to be an alternative version
of the optimal solution in the book, though I think mine is slightly easier to
reason about as I don't have to treat more than one 0s next to each other as a
special case
"""

def longest_sequence(n):
    s = bin(n)[2:]
    max_count = current_count = 0
    zero_index = -1
    for i, char in enumerate(s):
        if char == '1':
            current_count += 1
        else:
            if zero_index == -1: # first 0
                zero_index = i
                current_count += 1 # assume it's turned to a 1
            else:
                max_count = current_count if current_count > max_count else max_count
                current_count = i - zero_index
                zero_index = i
    return max_count

print(longest_sequence(20)) # 3
print(longest_sequence(210)) # 4
print(longest_sequence(256)) # 2
print(longest_sequence(257)) # 2

