"""
Approach 1 (Brute force): Start from one side and swap bits by their
neighbours (moving 2 steps forward each time) But this takes too many
instructions.

Approach 2: Make 2 masks, one to capture odd bits, one for even bits.
I start my numbering of bits from 0 and the right side.
Shift the odds down by 1, shift the evens up by 1 and then OR them together.
(This comes from how the position swaps happen i.e. 1->0 3->2 0->1 2->3)
If the binary representation has an odd number of bits, we may want to ignore
the odd one, MSB, or swap it with a leading 0. I'll assume swapping with
leading 0 is the desired behaviour. This is the current behaviour as the MSB
would be at an even position and shifted up and then ORed with a 0. (No need to
worry about 32bit or 64bit limitations as Python ints are of arbitrary
position)

The only remaining issue is mask construction.
Even mask example: 1010101 = 2**0 + 2**2 + 2**4 + 2**6 + .. where the greatest
term's exponent is ceil(len(binary representation)/2)
If the mask is longer, there won't be any issues, so we don't have to worry to
get the length exact, just at least as big

This is a geometric sum so we can have the number through a O(1) calculation
(After Googling, as I've forgotten geometric sum formula: S = (a(1-r**n))/(1-r)
where a is the first term, n is the number of terms and r is the ratio between
terms (multiplier))

After reading book solution: Int size of 32 is assumed, and the mask
calculation is done manually i.e. the masks 0xaaaaa..a and 0x55..5 are hard
coded. My solution uses the least space and works with Python numbers that can
have infinite precision, though it quite a bit more instructions. (Simplifying
the solution (using fewer instructions) is straightforward after restricting
the length of the input, so I haven't added the alternative solution)
"""

import math
def swap_bits(a):
    binary_length = math.floor(math.log(a,2)) + 1
    mask_length = math.ceil(binary_length/2)
    even_mask = (4**mask_length-1)//(3) # integer division
    # instead of 2*(4**mask_length-1)/(3) we can just shift
    odd_mask = even_mask << 1

    even = a & even_mask
    odd = a & odd_mask

    return (even << 1) | (odd >> 1)

print(bin(swap_bits(0b101)))
print(bin(swap_bits(0b111)))
print(bin(swap_bits(0b001)))
print(bin(swap_bits(0b1001)))
print(bin(swap_bits(2**3)))