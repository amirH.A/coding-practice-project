"""
(n & (n-1)) == 0 is checking that n and n-1 ANDed together are zero, i.e. they
have no overlap [of 1s].

If a number, n, and its previous number have no overlaps, the n must have
overflowed to the next bit position, i.e. n is of the form 2**i for some i and
n-1 has the form 2**i - 1, i.e. a sequence of all 1s.
(i.e. n is a power of 2 or 0)

As supporting evidence for this, notice that when a number is increased by 1,
if its MSB (most significant bit) remains a 1 it will be shared by the previous
value, but if it changes from a 1 to 0, then it must mean it was a sequence of
1s and n+1 is of the form 10..0 (i.e. 2**i)

Update after reading book solution:
My solution is correct though I derived it by trying to think of a case
where n and n-1 have no overlapping 1s, and the above case is the first
thing I thought, then after a bit more thought I deduced that this the only
case that it can happen. (The derivation in the book seems less natural to me))
"""