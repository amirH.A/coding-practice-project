"""
My first thought is that the two ends can be be used for accessing two stacks
if we divide the stack in half and have the middle be the end point
for both.

If the goal is to use an array to implement three 3 stacks despite the
performance issue, we could divide the array in three and store the positions
of start and end of each stack.
We'd also keep the index of 'top' for each stack and addition would
be adding elements at that position and increasing the 'top'.
We may also want to add the capability of extending and shortening different
stacks (which would requite copying of elements)

After book solution: The only caveat with my suggestion is handling of
extension of the third array which can potentially go beyond the en of array.
We could circle back to the start of the array and treat the array as a
circular buffer

(Due to time limitation I won't implement the solution now)
"""