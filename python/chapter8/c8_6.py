"""
We may want to implement a tower object that always checks when something is
added and removed to its stack to ensure the constraints are met.

Ech disk can be represented as number where a larger number means a larger disk
Let's say the numbers start from 1 to N for N disks

"""
class Stack:
    """
    End of the stack/list is the top of the stack, so I append to add and pop last
    element to pop. This is to avoid any potential copying when removing
    elements as list are dynamic arrays
    """

    def __init__(self):
        self.stack = []
    def push(self, a):
        self.stack.append(a)
    def pop(self):
        return self.stack.pop()
    def is_empty(self):
        return len(self.stack) == 0
    def peek(self):
        if not self.is_empty():
            return self.stack[-1]
        else:
            # instead of returning something like -1, so that I can ignore the
            # exception and catch bugs earlier
            raise Exception("Empty stack")

class Tower:
    def __init__(self):
        self.stack = Stack()

    def add(self, n):
        if self.stack.is_empty():
            self.stack.push(n)
            return True # was able to add
        else:
            if self.stack.peek() > n:
                # larger value means bigger disk, so safe to add
                self.stack.push(n)
                return True
            else:
                return False # notice that the the disk sizes are assumed to be
                # distinct so if 2 are equal we return False on the assumption
                # that it's invalid
    def remove(self):
        return self.stack.pop()

    def move_to(self, t):
        t.add(self.remove())

def move(n, origin, destination, buffer):
    if n == 0:
        return
    # move from top n-1 disks from origin to buffer, using destination as
    # buffer
    # by the end we expect destination to be empty
    move(n-1, origin, buffer, destination)

    # move the nth disk (last disk) from origin to destination
    origin.move_to(destination)

    # move the rest of the disks from buffer to destination, using origin as a
    # buffer
    move(n-1, buffer, destination, origin)


def solution(n):
    s1 = Tower()
    s2 = Tower()
    s3 = Tower()

    # initialise the first stack
    for i in range(n, 0, -1):
        s1.add(i)

    move(n, s1, s3, s2)
    return (s1, s2, s3)

n2 = solution(2)
print(n2[0].stack.stack, n2[1].stack.stack, n2[2].stack.stack )

n6 = solution(6)
print(n6[0].stack.stack, n6[1].stack.stack, n6[2].stack.stack )

