"""
1 1 1 ... 1
1 2 1 ... 1
1 2 2 ... 1
..
3 3 ...


"""

def number_of_ways(n, d):
    if n == 1 or n == 2:
        return n
    if n == 3:
        return 4
    if d[n]:
        return d[n] # return d[n] if it's not 0, meaning it has been
        # initialised
    d[n] = number_of_ways(n-1, d) + number_of_ways(n-2, d) +\
     number_of_ways(n-3, d)

    return d[n]

def n_ways(n):
    return number_of_ways(n, [0]*(n+1))

# Constant space
def number_of_ways2(n):
    if n == 1 or n == 2:
        return n
    if n == 3:
        return 4
    a = 1
    b = 2
    c = 4
    for i in range(4,n+1):
        (a, b, c) = (b, c, a + b + c)

    return c

print(n_ways(42))
print(number_of_ways2(42))
