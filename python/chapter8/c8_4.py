"""
Approach 1: Each element is chosen or not chosen, so have a recursive function
that chooses each element half the time and doesn't choose it the other half
and recursively build a set.

Approach 2: Construct the power set for the set minus the last element then add
that element to all possible positions along with the previous values.
E.g. for {a, b, c} build power_set({a, b}) which is {}, {a}, {b}, {a,b}
Then add c to all previous element while keeping the old ones as well:
{}, {a}, {b}, {a, b}, {c}, {a, c}, {b, c}, {a, b, c}

This approach is in a way like approach 1, but we first build the power set
where we never choose the later elements, then choose the later elements and
add them, e.g. if the original set was {a, b, c, d} we'd just add d to all
previous elements' copies as if in the first half d was never chosen, in the
second half d is always chosen. Notice that this always double the number of
sets when adding new elements which is consistant with how the number of sets
in a power set is 2^n where n is the number of elements in the original set
(so moving from n=3 to n=4 is expected to double the number of sets)
"""
# A set d, is represented as a list and we'll return a list of the sets, i.e.
# a list of lists
def power_set(d):
    if len(d) == 0:
        return [[]]

    a = power_set(d[:len(d)-1]) # power set of the set without the last element

    new_sets = [] # avoid modifying a while traversing it
    for e in a:
        new_set = e.copy() # make a copy to ensure the original set is
        # still there as well
        new_set.append(d[len(d) - 1]) # Add this last element that was removed
        new_sets.append(new_set)

    return a + new_sets # concatenate the sets

print(power_set([0]))
print(power_set([0,1,2]))
print(power_set([0,1,2,3]))