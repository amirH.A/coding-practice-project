"""
Approach 1: Construct the permutations
of a string of length n-1, add the nth character to every position in each
of the previous strings

a: a
ab: ab, ba
abc: abc, acb, bac, bca, cab, cba
  == cab, acb, abc, cba, bca, bac  adding c to every position of ab and ba
"""

def string_perm(s):
    # print("called with", s)
    if len(s) == 0:
        return [""]
    perms = string_perm(s[:len(s)-1])

    d = []
    for p in perms:
        for i in range(0,len(p)+1):
            new_string = p[:i] + s[-1] + p[i:]
            # print("s is", s, "s[-1]:",s[-1])
            d.append(new_string)

    return d

print(string_perm("abc"))
print(string_perm("abcd"))
