"""
The question only wants 1 magic index so don't have to worry about the case
where there are multiple ones, e.g. if the values where also from 0 to n-1
every index would be a magic index.

My initial feeling is that a divide and concur approach will work.

If we visit
the middle of the array and the value is greater than the index then the value
of all indexes above it will also be greater than their index (as they can need
to at least increase by 1, using the distinct property.)
So we'll need to look in the lower half.

If the mid value is lower than its index, by a similar argument we'll need to
look in the upper half.

If the mid value is equal to its index we are done and return that index

(So just binary search)

TODO: Implement the changes for the follow up
"""

def find_magic_index(d):
    low = 0
    high = len(d) - 1
    while low <= high:
        mid = (low+high)//2
        # print("low:", low, "high:", high, "mid:", mid, d[mid])
        if d[mid] == mid:
            return mid
        elif d[mid] > mid:
            high = mid - 1
        else:
            low = mid + 1

    return -1 # No magic index found

print(find_magic_index([-10,-2,-1,3,100,1000]))
print(find_magic_index([0,3,4,5,100,1000]))
print(find_magic_index([-6,-5,-4,-3,4,1000]))