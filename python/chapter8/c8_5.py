"""
Approach 1: Add the larger int, to it self as many times as the smaller int
(in an attempt to try to use as few operation as possible)
The issue with above is that bit shifting has also been allowed which means we
could easily multiply by 2 which would lead to the answer after fewer
operations for the many cases such as when both numbers are large

Approach 2: Choose min and max the same as above but if/when min is a multiple
of 2, i.e. the last bit of it is 0, shift it down by 1 (in effect halving it)
and shift max up by 1 (multiplying it by 2). The check for min being even is so
that we only halve when we know halving by shifting will be exact (e.g. halving
3 would give 1 which is not exact)

I believe the 2nd approach improves on 1 but always choosing min and testing if
it's even The above

Update after reading the book solution: The division by 2 is done on all
numbers (even or odd) and in the case of odd they are handled differently
e.g. let k be odd then we know k = 2n+1 for some n, so k*p = (2n+1)*p = 2np + p

This means one could still halve smaller, and double larger and in case of odd,
just add the previous value of larger. (The book solution uses stack space
through function calls which means it has a space complexity of O(log s) as
well where s is the smaller number; my old solution has a constant space
complexity)

Mult2 is the implementation of the improved book solution (third version in the
book)
"""

def mult(a, b):
    smaller = min(a, b)
    larger = max(a, b)

    while smaller%2 == 0:
        smaller >>= 1
        larger  <<= 1

    # print("smaller", smaller, "larger", larger)
    sum = larger
    for _ in range(0,smaller-1): # only add min-1 times as sum was initialised
        # to larger
        sum += larger

    return sum

def mult2(a, b):
    smaller = min(a, b)
    larger = max(a, b)
    return mult2_helper(smaller, larger)

def mult2_helper(smaller, larger):
    if smaller == 0:
        return 0
    if smaller == 1:
        return larger

    s = mult2(smaller>>1, larger)
    if smaller%2 == 0:
        return s+s
    else: # smaller was odd
        # in this case smaller = 2n+1 for some n, where n = smaller>>1
        # let larger be p and smaller be q. p*q = (2n+1)*q = 2(qn) + q
        # = qn + qn + q
        return s+s+larger

print(mult2(10,53))
print(mult2(9,4))