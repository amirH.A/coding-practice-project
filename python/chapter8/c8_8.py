"""
Approach 1: Get the count of each character and make a new string which only
contains 1 instance of each character. Then return the perms on that

"""

def string_perm(s):
    # print("called with", s)
    if len(s) == 0:
        return [""]
    perms = string_perm(s[:len(s)-1])

    d = []
    for p in perms:
        for i in range(0,len(p)+1):
            new_string = p[:i] + s[-1] + p[i:]
            # print("s is", s, "s[-1]:",s[-1])
            d.append(new_string)

    return d

def string_perm_with_dup(s):
    d = {}
    for c in s:
        d[c] = d.get(c,0) + 1

    new_s_list = [] # instead of new_s="" as strings are immutable, so avoiding
    # n copying
    for c in d:
        new_s_list.append(c)

    new_s = ''.join(new_s_list)
    return string_perm(new_s)

print(string_perm_with_dup("aaabaa"))
print(string_perm_with_dup("abc"))
print(string_perm_with_dup("aaaabbbc"))
