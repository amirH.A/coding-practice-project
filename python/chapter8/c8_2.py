"""
This is a path finding problem which I think are best done through graph search
But given the context, I can think of a dynamic programming like solution that
works backwards from the goal and tries to reach the robot. Starting from the
robot will work as well but generally for mazes when starting from the end
you'll find shorter dead end paths

Thinking more carefully about it, I think the problem is meant to suggest
implicit backtracking through function calls, which may be easier to reason
about when starting from the robot.

I assume I'm given a grid filled with 1s and 0s where 0s mean "off limits"

Approach 1: Recursively try going right until off limit or an edge,
then try going down.

If no possible further moves fail, and backtrack implicitly by going back in
the stack

Update after reading the solution, it looks like the book was actually looking
for the starting from the goal approach. (TODO: implement that as well)
"""

def find_path(grid, path=None, position=(0,0)):
    if path is None:
        path = []

    # at the goal
    if position == (len(grid[0]) - 1 , len(grid) - 1):
        return (True, path)

    # go right if possible
    if (position[0] + 1 < len(grid[0])) and grid[position[0]+1][position[1]]:
        r = find_path(grid, path.append("right"), (position[0]+1, position[1]))
        if not r[0]:
            path.pop() # if failed, remove this direction
        return r
    # go down if possible
    if (position[1] + 1 < len(grid)) and grid[position[0]][position[1]+1]:
        r = find_path(grid, path.append("down"), (position[0], position[1]+1))
        if not r[0]:
            path.pop()
        return r

    return (False, None)

# The code looks a bit duplicated so it can be improved


