"""
Approach 1: recursive, inefficient [exponential]
(This turned out to be the same/very similar as the first solution of the book)

Approach 2: the height procedure can check the sub trees are also balanced
If not raise exception and add handler to the caller, is_balanced.

(The book uses return values instead of exceptions, exception implementations
tend to be slow and one can argue using Exception is not correct here as
not being balanced is a possible outcome. But when using exception we don't
have to explicitly check a special return type and pass it upwards)
"""
class Node:
    def __init__(self, val, leftChild=None, rightChild=None):
        self.value = val
        self.leftChild = leftChild
        self.rightChild = rightChild
        self.visited = False

def height(tree):
    if tree is None:
        return 0
    return max(height(tree.leftChild), height(tree.rightChild)) + 1
def is_balanced(tree):
    if tree is None:
        return True
    if not is_balanced(tree.leftChild) or not is_balanced(tree.rightChild):
        return False

    if abs(height(tree.leftChild) - height(tree.rightChild)) < 2:
        return True
    return False

class NotBalanced(Exception):
    pass

def height_2(tree):
    if tree is None:
        return 0
    left_height = height_2(tree.leftChild)
    right_height = height_2(tree.rightChild)
    if abs(left_height - right_height) > 1:
        raise NotBalanced()
    else:
        return max(left_height, right_height) + 1


def is_balanced_2(tree):
    if tree is None:
        return False
    try:
        if height_2(tree) > 0:
            return True
    except NotBalanced:
        return False
"""
         4
       /   \
     3       5
    / \      /
   1   2    9
             \
             10
"""
n1 = Node(1)
n2 = Node(2)
n3 = Node(3, n1, n2)
n10 = Node(10)
n9 = Node(9, None, n10)
n5 = Node(5, n9)
n4 = Node(4, n3, n5)
print(height(n4))
print(height(n10))
print(height(n9))
print(height(n5))
print(height(n3))
print(is_balanced(n4))
print(is_balanced(n5))
print(is_balanced(n3))

print(is_balanced_2(n4))
print(is_balanced_2(n5))
print(is_balanced_2(n3))
