class Node:
    def __init__(self, val, leftChild=None, rightChild=None):
        self.value = val
        self.leftChild = leftChild
        self.rightChild = rightChild
        self.visited = False
class Tree:
    def __init__(self, root):
        self.root = root

# LinkedList implementation
class Item:
    def __init__(self, val, next=None):
        self.value = val
        self.next = next

    # Add support for traversing the LinkedList easily using constructs
    # such as for i in Item:
    def __iter__(self):
        current = self
        while current is not None:
            yield current.value
            current = current.next

"""
Approach 1: Breadth first search approach where we travel down and keep a list
of all linked lists; add the values of nodes at each level to that list
and when no more elements left, return this list of LinkedLists
Update after reading solution: correct approach, but I was not keeping
an extra dictionary for no reason while I could just use the currentLinkedList
as parents. Updated the code.

"""

def createLinkedList(tree):
    linkedLists = []

    currentLinkedList = Item(tree.root)
    while currentLinkedList: # pythonic way of checking there are elements
        linkedLists.append(currentLinkedList)
        parents = currentLinkedList
        currentLinkedList = None

        for n in parents:
            # print("HERE:", n.value)

            if n.leftChild:
                currentLinkedList = Item(n.leftChild, currentLinkedList)
                # print("n left child")
            if n.rightChild:
                currentLinkedList = Item(n.rightChild, currentLinkedList)
                # print("n right child")
                # currentLinkedList might be None or the left child if it
                # exists

    return linkedLists

"""
         4
       /   \
     3       5
    / \      /
   1   2    9
             \
             10
"""
n1 = Node(1)
n2 = Node(2)
n3 = Node(3, n1, n2)
n10 = Node(10)
n9 = Node(9, None, n10)
n5 = Node(5, n9)
n4 = Node(4, n3, n5)
tree = Tree(n4)

linkedLists = createLinkedList(tree)
for i, list in enumerate(linkedLists):
    print(f"Level {i}: ")
    while list.next is not None:
        print(list.value.value, end="\t")
        list = list.next
    print(list.value.value) # print the last item (item before None)




