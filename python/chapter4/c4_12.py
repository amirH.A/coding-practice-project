"""
Approach 1 (Brute force approach): start from each node go downards and count
the number 0of times sum is equal to the target.
Time complexity: O(n log n), space complexity: O(1)


Approach 2: There's only 1 path from the root to any node; the paths that
don't start at the root will still be a subset of that path.

Since the values are stored at each node and not edge I assume a path with
length 0 is valid, i.e. if a node has a value equal to the target that counts
as 1 path.

We could store a list of all the sums possible at each node. Start by adding
the node value to the list and then add the node value to each element of the
parent list and them to this node's list.

At the end traverse through all nodes and count how many nodes contain the
target in their list.

Time complexity: O(n), space complexity O(n^2) (a tighter bound can be
achieved, I got as far as O(n log(n!))) but not sure if that's right


After reading book solution: <too much detail to summarize here, it acheives
O(n) time complexity and O(log n) space complexity in a balanced tree by
reducing the duplicated work of adding sums from the root each time.

"""