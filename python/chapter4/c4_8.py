"""
My first thought was to store the parents of each in a dict and move up each
one by one, as we don't know anything about their initial heights. But the
questions mentions avoiding additional storage of nodes.


A work around might be to store from which direction the node propagated
upwards i.e. store a list of right/left such that one can get to the original
node from any ancestor.
One approach is to propagate upwards and store the direction for each node
simultaneously. At each step check if the nodes are the same (very small chance
as both initial nodes must have been at the same height). Propagate upwards
until the root is reached. (If a common node is reached that isn't the root,
this must be the first common ancestor, so return)
If the root is reached use the list of directions for each node and go
downwards towards each node simultaneously. When the next node is different,
return the previous node as that was the common ancestor.

The above approach still requires storing the directions; O(log N) space
complexity.

One can make a simple modification to keep the space complexity to O(1) by
picking one of the nodes, moving one step upwards but moving upwards up to the
root for the other node and checking the nodes along they way for equality;
time complexity will be O(log^2 N)

Book solution: The O(log^2 N) approach is not mentioned, but there's an
improved version of the first one. We can find the heights of the each given
node and move up the deeper node and then check the nodes for equality and move
both up 1 step at the time and check for equality. First common node is the
common ancestor. This has time complexity O(log N) and space complexity O(1)
But it requires the node to contain a reference to the parent (which I
assumed for my solutions)

When there are no references to the parent, we can start from the root and
check which side each node is. If on different sides, the current node is the
common ancestor otherwise move down.

[The book offer more solutions which optimise, but not asymptotically, I think
the space complexity might be higher in those solutions due to the concurrent
number of function calls, which essentially store quite a bit of state
implicitly]

"""