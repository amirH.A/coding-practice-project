"""
Initial thoughts: I think there's an efficient way of doing it using the
attributes of how in-order works.
The brute force would be to just go up to root, print in order, find where the
node was and print what's after it (This won't be able to handle duplicate
values, i.e. in case of being given a node that has a duplicate we won't be
able to find it successor by just comparing values [we would have to modify
it to comapre the node objects])
It's complexity is still O(N)

Better approach: If the node has a right child, its successor is the left most
child of the right child
Otherwise if the node is the left child, the parent is the successor, if it's the
right child, the successor is the parent of the first ancestor that was a
left child.

Another edge case, if we go up to the root from the right subtree, there is no
successor (i.e. we started from the last element)
Complexity O(log N) where N is number of nodes in the tree
"""
class Node:
    def __init__(self, val, leftChild=None, rightChild=None, parent=None):
        self.value = val
        self.leftChild = leftChild
        self.rightChild = rightChild
        self.parent = parent

def next_successor(n):
    if n.rightChild is not None:
        next = n.rightChild
        while next.leftChild is not None:
            next = next.leftChild
        return next

    # also handles the edge case of returning None for  last element
    parent = n.parent
    current = n
    while parent is not None and parent.leftChild is not current:
        current = parent
        parent = current.parent

    return parent

def inorder(tree):
    if tree is None:
        return
    # print("Tree ", tree.value)
    inorder(tree.leftChild)
    print(tree.value, end="\t")
    inorder(tree.rightChild)

"""
         4
      /     \
     3       10
    / \     /
   1   5   8
            \
             9
"""
n1 = Node(1)
n5 = Node(5)
n3 = Node(3, n1, n5)
n1.parent = n3
n5.parent = n3

n9 = Node(9)
n8 = Node(8, None, n9)
n9.parent = n8

n10 = Node(10, n8)
n8.parent = n10

n4 = Node(4, n3, n10)
n3.parent = n4
n10.parent = n4

inorder(n4)
print("")
print(next_successor(n1).value) # 3
print(next_successor(n4).value) # 8
print(next_successor(n5).value) # 4
print(next_successor(n3).value) # 5
print(next_successor(n8).value) # 9
print(next_successor(n9).value) # 10
print(next_successor(n10)) # None
