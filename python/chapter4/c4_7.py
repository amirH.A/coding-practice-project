"""
This seems to be a classic topological sort, but I haven't seen that in a few
years so I'll try to reconstruct something.

Approach 1: Make each project a Node where each node has a list of dependencies
Then add the dependencies and start by going through all nodes trying to find
a node that has no dependencies. Add that to the list of builds (at the end)
and remove it from the dependencies of every other node (thus a link back to
children can be useful to avoid traversing the array again)
Repeat this cycle
If at any run, no node without dependencies is found, return error
Otherwise when all node have been added to the builds, return the builds

"""

class Node:
    def __init__(self, val):
        self.value = val
        self.dependants = [] # references to nodes
        self.dependencies = [] # just the strings

def build_order(list):
    project_nodes = {} # map of project name to its created Node
    for tuple in list:
        p1 = None
        p2 = None
        if tuple[0] in project_nodes:
            p1 = project_nodes[tuple[0]]
        else:
            p1 = Node(tuple[0])
            project_nodes[tuple[0]] = p1

        if tuple[1] in project_nodes:
            p2 = project_nodes[tuple[1]]
        else:
            p2 = Node(tuple[1])
            project_nodes[tuple[1]] = p2

        if p1.value not in p2.dependencies:
            p2.dependencies.append(p1.value)
            p1.dependants.append(p2)
            # two way relation, so if dependencies not set,
            # dependants wouldn't be either

    build_order = []
    while len(build_order) < len(project_nodes.values()):
        made_progress = False
        for node in project_nodes.values():
            if node.dependencies == [] and node.value not in build_order:
                made_progress = True
                build_order.append(node.value)
                for i in node.dependants:
                    i.dependencies.remove(node.value)
                break
        if not made_progress:
            raise Exception("No plausible build order")

    return build_order


print(build_order([
                ("a", "d"),
                ("f", "b"),
                ("b", "d"),
                ("f", "a"),
                ("d", "c")
                ]))
