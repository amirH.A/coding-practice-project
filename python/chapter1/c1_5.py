"""
First approach: Take one string, insert all operations on it and check
if they match.
Notice that the length of the strings will determine which operation
should be attempted. i.e.
* 1 less -> insertion
* 1 more -> deletion
* equal  -> replacement


"""

def isOneAway(a, b):
    if len(a) == len(b) + 1:
        return isOneAwayByDeletion(a,b)
    elif len(a) == len(b) -1:
        return isOneAwayByInsertion(a,b)
    elif len(a) == len(b):
       return isOneAwayByReplacement(a,b)
    else:
        return False

def isOneAwayByDeletion(a, b):
    temp = ""
    for char in a:
        temp = a.replace(char, "", 1) # Only replace first instance
        if temp == b:
            return True

    return False

def isOneAwayByInsertion(a,b):
    """Check that they have all the same characters except 1"""
    d = {}
    for (char in a):
        d[char] = d.get(char, 0) + 1
    for (char in b):
