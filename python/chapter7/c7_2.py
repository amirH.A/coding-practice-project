from enum import Enum

class EmployeeRole(Enum):
    respondant = 1
    manager = 2
    director = 3

class Employee:
    def __init__(self, role, name, id):
        # this role approach as opposed to subclassing as in the book, has the
        # advantage of easier role updating, addition and removal
        self.role = role
        self.name = name
        self.id = id
        self.busy = False
        # could add other attributes such as salary, start time, department, ..
    def __eq__(self, other):
        # for easy removal of Employees
        return self.id == other.id

    def set_busy(self):
        self.busy = True
    def set_free(self):
        self.busy = False
    def is_free(self):
        return not self.busy

    # implementation of deciding wether the call can be handled or not
    # for these demonstration purposes a call is a number; self.role.value is
    # a number between 1 and 3 as defined by EmployeeRole Enum which roles are
    # a type of
    def can_handle_call(self, call):
        return call < self.role.value * 10

    # return whether the call was handled successfully or not
    def assign_call(self, call):
        self.set_busy()
        handle_result = self.can_handle_call(call)
        self.set_free()
        return handle_result


class CallCentre:
    def __init__(self):
        # three lists to avoid searching through a single list of employees
        # (more efficient time wise)
        # might even want to control the Employee going into busy state from
        # this class and have 6 lists, a list for free employees of that role
        # and another list for busy employees of that role, e.g.
        # busy_respondants and free_respondants
        # This added complexity is not worth it here, but might be desirable if
        # there are too many Employees and searching through them to find free
        # ones is too expensive
        self.respondants = []
        self.managers = []
        self.directors = []

    # If below is needed, they need to be modified to add/remove employee from
    # the appropiate list

    # def addEmployee(self, employee):
    #     self.employees.append(employee)

    # def removeEmployee(self, employee):
    #     try:
    #         self.employees.remove(employee)
    #     except ValueError:
    #         print("Error: No such Employee.")

    def dispatch_call(self, call, level):
        handlers = []
        if level == 1:
            handlers = self.respondants
        elif level == 2:
            handlers = self.respondants
        elif level == 3:
            handlers = self.directors
        else:
            return False # Can't handle the call, e.g. if the director can't
            # handle it either

        for e in handlers:
            if e.isFree():
                if not e.assign_call():
                    self.dispatch_call(call, level+1) # pass upwards
                else:
                    # call was handled
                    return True

        return False # No one was available to handle the call, caller expected
        # to call later, might want to add a queue and free employees process
        # the queue
