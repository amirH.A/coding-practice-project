"""
Assuming a typical card card deck that contains 52 cards and 4 suits: spades,
clubs, hearts, diamonds

Each card has a name and a value. The name is the value in case of 2-10,
for 1, it's "Ace".

(I'm not familiar with Black Jack, so represented it but I think the basic
structure below is fairly general and can easily be subclassed and overriden)
"""
from enum import Enum

class SuitName(Enum):
    Spades = 1
    Clubs = 2
    Hearts = 3
    Diamonds = 4

class Card:
    def __init__(self, name, value, suit):
        self.name = name
        self.value = value
        self.suit = suit

class Deck:
    # A deck of cards is a list of cards
    cards = []

    @staticmethod
    def initDeck():
        for suit_name in SuitName:
            Deck.cards.append(Card("Ace", 13, suit_name))
            for i in range(2,11):
                Deck.cards.append(Card(str(i), i-1, suit_name))
            Deck.cards.append(Card("Jack", 10, suit_name))
            Deck.cards.append(Card("Queen", 11, suit_name))
            Deck.cards.append(Card("King", 12, suit_name))

    @staticmethod
    def printCards():
        for card in Deck.cards:
            print(card.name,":", card.value, "\t", card.suit)

Deck.initDeck()
Deck.printCards()
