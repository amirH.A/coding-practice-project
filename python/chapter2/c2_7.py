from c2_x import Node
"""
First approach: Store the nodes of the first LinkedList in a set
Then go through the second LinkedList and if any nodes are in the set, then
they interset

"""

def doIntersect(a, b):
    d = set()
    node = a # don't lost reference to a
    while node is not None:
        d.add(node)
        node = node.next
    while b is not None:
        if b in d:
            return True
        b = b.next
    return False


node23 = Node(23)
node22 = Node(22, node23)
node21 = Node(21, node22)

node4 = Node(4)
node3 = Node(3, node4)
node2 = Node(2, node3)
node1 = Node(1, node2)

node23.next = node3

print(node1.printList())
print(node21.printList())
print(doIntersect(node1, node21))

