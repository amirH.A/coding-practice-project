class Node:
    def __init__(self, val, next=None):
        self.val = val
        self.next = next

    def printList(self):
        vals = []
        n = self
        while n is not None:
            vals.append(str(n.val))
            n = n.next

        return " ".join(vals)

