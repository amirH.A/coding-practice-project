from c2_x import Node

"""
Checking against val won't work, will need to check against the class ref

[This can be done through the `is` keyword in python (by default, object
comparison in python compares their id, so using equality comparison,
`==`, will also work)]


Edge case: If there are no loops, it should be detected and avoid infinite
processing

Update After reading the solution, my space complexity is O(N) but otherwise
a much simpler solution. As the question doesn't specify any restrictions on
space and neither do the solutions, I wonder if this solution has a bug?
(https://github.com/careercup/CtCI-6th-Edition-Python/issues/92)
"""

def startOfLoop(n):
    d = set()
    while n is not None:
        # I believe `in` will just call the default __eq__ which will be ok
        # see notes at top
        if n in d:
            return n
        else:
            d.add(n)
        n = n.next


node5 = Node(5)
node4 = Node(4, node5)
node3 = Node(3, node4)
node2 = Node(2, node3)
node1 = Node(1, node2)
node5.next = node3

# Note: DO NOT CALL Node.printList, since it doesn't handle cycles, it will
# hang and you may have to restart your machine

print(startOfLoop(node1).val)