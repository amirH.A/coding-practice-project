# Coding Practice Project

## Introduction

My solutions to problems in Cracking the Coding Interview.
I start by writing my thoughts/design and then implement them before checking
the solutions.

Due to my time constraints, I may not implement a solution for every question.
(Currently I'm planning to go through as much of the book as possible in 2
weeks, thus, I'll skip a portion of solution. However, I'm hoping I'll come
back to them and add more solution)

## Style Guide

79 character width limit

## TODO

- [ ] UnitTests: From my research py.test seems like the ideal solution,
 not too much boilerplate code and powerful enough. ( I also prefer usage of a single assert instead of different types of asserts) These unit tests should be in their own file.
- [ ] Enforce styles through pipelines
- [ ] For test cases, create a separate repo that has a judge with tests cases
 That repo can be made a submodule of this one and pipelines can even be added
 to automatically invoke the judge for a solution (separate repo to promote
 contributions from others)


I aim to make this a collaborative repo with detailed explanations for the problems. (though this may not be true at first)
If you'd like an improved explanation to a problem feel free to open an issue requesting one.
Additionally if you have a better solution or would like to contribute in any way feel free to make merge requests.
